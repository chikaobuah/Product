require 'gitlab'
require 'yaml'
require 'erb'

class IssueCreator
    def run()
        connect()
        puts 'Loading content from config/issues.yml'
        @issues = YAML.load_file('config/issues.yml')
        puts 'Loading content from config/sections.yml'
        @sections = YAML.load_file('config/sections.yml')
        puts 'Content loaded successfully.'
        puts 'Walking issues to create...'
        @issues['issues'].each do |issue|
            puts '* ' << issue['name'] << ' should be created on the ' << issue['day_of_month'].to_s << ' day of the month.'
            today_day = Date.today.strftime("%-d")
            puts '** Today is the ' << today_day
            if today_day == issue['day_of_month'].to_s
                create(issue['name'],issue['template'],issue['scope'])
            end
        end
        puts 'Run Complete.'
    end
    
    def connect()
        puts "Connecting to GitLab..."
        @gitlab = Gitlab.client(
            endpoint: 'https://gitlab.com/api/v4',
            private_token: ENV['GITLAB_API_PRIVATE_TOKEN']  
        )
        @user = @gitlab.user
        puts "Connection successful. Connected user email: " << @user.email
    end

    def create(name, templateName, scope)
        if scope == 'section'
            @sections['sections'].each do |section|
                ## Adjust this to create the description based on template and section variables
                template = File.open('templates/'<<templateName<<'.erb').read
                puts '** Template loaded...'
                description = ERB.new(template,0,'>').result(binding)
                puts '** Description generated...'
                @gitlab.create_issue(ENV['GITLAB_PROJECT_ID'], name + ' ' + section['name'] ,{description: description, assignee_id: @user.id})
                puts  '** ' << name << ' issue created.'
            end
        end
    end
end

if $PROGRAM_NAME == __FILE__
    issue_creator = IssueCreator.new
    issue_creator.run
end