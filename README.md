# GitLab Product Department Repository
* [Handbook](https://about.gitlab.com/handbook/product/)

## GitLab Product Process

This repository also includes code to automate the scheduled creation of issues for the Product team.

## Usage

Update issues.yml and section.yml with the appropriate variables:
* **Issue Title** = Issue.name + Section.name
* **Issue Description** = Will parse template in /templates director and render as markdown
* **Date created** = Will create on the daily run that corresponds with the Issue.day_of_month
* **Issue.scope** determines at what level to create issues, see limitations below

## Running Locally
To run this locally you'll first need to set variables for a GitLab Project ID to create issues in AND a personal access token used to create the issue. 
```
export GITLAB_API_PRIVATE_TOKEN=YYY
export GITLAB_PROJECT_ID=XXX
```

Once those have been set, you simple need to run the `create_issue.rb` script
```
bundle exec ruby create_issues.rb
```

## Limitations

* The only scope that is available is Section, we should consider adding Company (all Sectoins) and Groups that would allow for the creation of an issue per group. 
* In order to use multiple quick actions you have to add an extra line space between them
* This runs with @kencjohnston's token at the moment