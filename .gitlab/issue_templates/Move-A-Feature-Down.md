##  Proposal
<!-- Link to the public issue where all content for the proposal of what specifically will be moving between tiers should exist. -->

## Rationale
<!-- Include rationale for the move. This section can contain multiple points such as buyer-based tiering conformance, critical customer request, or other business drivers. -->

## Financial Model
<!-- Copy and adjust the Pricing Tier Adjustment Model Template adding relevant details for this move. Summarize the resulting output in this issue. -->
* [Pricing Tier Adjustment Revenue Model - TEMPLATE](https://docs.google.com/spreadsheets/d/1VulFAyL4mfkpNEH5-YeOJsfWqDBlWIShfCr8Fjf8ibo/edit)

## Target Customer Interviews
<!-- Determine a list of affected customers and prospects and conduct interviews where appropriate. For customers, the act of involving GitLab advocates in the decision making process will smooth any future tension around the decision. Summarize the results of those conversations in this section. -->

## Process
### Content Creation and CEO and VP of Product Approval to Proceed
* [ ] @Mention in the comments your Product Marketing counterpart, the CRO, the CMO, the VP of Product, and the CEO
* [ ] If the change has significant financial or business model impact, partner with your Finance business partner and/or other critical team members to put together a rough sketch of the estimated impact. Update the issue with details. It is the responsibility of the Product Manager to have accurately identified, quantified, justified and communicated the business impact of the change prior to the proposal being submitted for approval.
* [ ] Share proposal for approval from the VP of Product and the CEO to proceed

### Company and Sales Review
* [ ] Share this issue for feedback with the rest of the team on the GitLab company call
* [ ] Join the weekly CRO Leadership Call, explain the proposal, and direct them to the issue to provide feedback. Please ask the appropriate EBA for an invitation. If attending the meeting is not feasible, ping the CRO on the issue and ask them to add it to an upcoming CRO Leadership meeting agenda.

### Final Approval
* [ ] After feedback has been provided update your recommendation and assign to the CEO for final approval

### After Approval
* [ ] Reference the confidential issue in the public issue with a brief statement summarizing the result
* [ ] Announce the result in the GitLab company call
* [ ] Announce the result in the WW Field Sales call

/confidential