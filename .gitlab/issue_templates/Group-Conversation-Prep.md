<!-- HOW TO USE THIS TEMPLATE

First, delete the headers pertaining to Sections (Ops, CI/CD) that aren't relevant for your group conversaton.

-->
Be sure to review and follow the most up-to-date [Group Conversation](https://about.gitlab.com/handbook/people-group/group-conversations/#for-meeting-leaders) instructions.

## References
* **[Ops Slides](https://docs.google.com/presentation/d/1-XtXosrDJ6-u-ILCzfmbRQu147QJ1RsVY8A6MwK809k/edit#slide=id.g156008d958_0_18)**

## Tasks (All)
* [ ] Block off calendar for the 30 minutes prior to the meeting
* [ ] Add the link to the slides to the GC Agenda Doc 24 hours in advance of the meeting
* [ ] Add the links to the GC Agenda and Slides to the #group-conversation slack channel, cross post to #company-announcements
* [ ] [Update this GC prep issue template](https://gitlab.com/gitlab-com/Product/edit/master/.gitlab/issue_templates/Group-Conversation-Prep.md) with any added items

## Tasks (Ops)
* [ ] Update Eng Team - @dcroft 
* [ ] Update Prod Team - @kencjohnston 
* [ ] Add Eng OKR Summary - @dcroft 
* [ ] Add UX OKR Summary - @awhite-gl
* [ ] Add Top Highlights Summary - All
* [ ] Update [Current and Next Quarter category maturity increment tracking](https://docs.google.com/presentation/d/1-XtXosrDJ6-u-ILCzfmbRQu147QJ1RsVY8A6MwK809k/edit#slide=id.g4f766bbfd7_1_0)
  * [ ] Health @sarahwaldner 
  * [ ] APM @dhershkovitch 
  * [ ] Serverless @nagyv.gitlab 
  * [ ] Kubernetes @danielgruesso 
* [ ] Add [things we're most excited about](https://docs.google.com/presentation/d/1-XtXosrDJ6-u-ILCzfmbRQu147QJ1RsVY8A6MwK809k/edit#slide=id.g63a7a349c9_0_19), link to issues or Epics
  * [ ] Health @sarahwaldner 
  * [ ] APM @dhershkovitch 
  * [ ] Serverless @nagyv.gitlab
  * [ ] Kubernetes @danielgruesso 
  * [ ] Review and Summarize @kencjohnston
* [ ] Update [Vision and Direction changes since last GC ](https://docs.google.com/presentation/d/1-XtXosrDJ6-u-ILCzfmbRQu147QJ1RsVY8A6MwK809k/edit#slide=id.g63a7a349c9_0_14)
* [ ] Create a view of the delta from previous GCs maturity plan
* [ ] Add Section SMAU Slide

## Tasks (CI/CD)
* [ ] Update product categories/maturity slide @jlenny (copy/paste from `direction/...`)
* [ ] Update who we are slide @jlenny
* [ ] Update screenshots on category maturity plan slide @jlenny
* [ ] Update screenshots on overestimated SMAU slide @jlenny
* [ ] Update hiring slide 
  * [ ] PM @jlenny
  * [ ] Design @nudalova
  * [ ] Engineering @darbyfrey
* [ ] Update throughput slide @darbyfrey
* [ ] Update strategy slides @jlenny 
* [ ] Update recent popular items @jlenny
* [ ] List recent published content (review @williamchia)
  * [ ] Verify CI @jlenny 
  * [ ] Verify Runner @DarrenEastman 
  * [ ] Verify Testing @jheimbuck_gl
  * [ ] Package @trizzi
  * [ ] Release Progressive Delivery @ogolowinski 
  * [ ] Release Release Management @jmeshell 
* [ ] Add topic slides (interesting items that are top of mind that deserve their own slide)
  * [ ] Verify CI @jlenny 
  * [ ] Verify Runner @DarrenEastman 
  * [ ] Verify Testing @jheimbuck_gl
  * [ ] Package @trizzi
  * [ ] Release Progressive Delivery @ogolowinski 
  * [ ] Release Release Management @jmeshell 
* [ ] Update what's coming next
  * [ ] Verify CI @jlenny 
  * [ ] Verify Runner @DarrenEastman 
  * [ ] Verify Testing @jheimbuck_gl
  * [ ] Package @trizzi
  * [ ] Release Progressive Delivery @ogolowinski 
  * [ ] Release Release Management @jmeshell 
* [ ] Update requests for contribution
  * [ ] Verify CI @jlenny 
  * [ ] Verify Runner @DarrenEastman 
  * [ ] Verify Testing @jheimbuck_gl
  * [ ] Package @trizzi
  * [ ] Release Progressive Delivery @ogolowinski 
  * [ ] Release Release Management @jmeshell 
* [ ] Final review @jlenny 

# Tasks (Enablement)
* [ ] Update product who we are / categories @joshlambert
* [ ] Update screenshots on category maturity plan slide @joshlambert
* [ ] Update screenshots on telemetry slide @joshlambert
* [ ] Update hiring slide 
  * [ ] PM @joshlambert
  * [ ] Design @jackib
  * [ ] Engineering @cdu1
* [ ] Add/Update throughput slide @cdu1
* [ ] Update priorities/strategy slides @jlenny 
* [ ] Update dogfooding slide @ljlane @fzimmer @deuley @joshlambert
* [ ] Add/Update recent popular items @joshlambert
* [ ] List recent published content @joshlambert
* [ ] Add topic slides (interesting items that are top of mind that deserve their own slide)
  * [ ] Distribution @ljlane
  * [ ] Ecosystem @deuley
  * [ ] Geo @fzimmer
  * [ ] Memory @joshlambert
  * [ ] Database @joshlambert
  * [ ] Search @phikai @joshlambert
* [ ] Accomplishments & Plans
  * [ ] Distribution @ljlane
  * [ ] Ecosystem @deuley
  * [ ] Geo @fzimmer
  * [ ] Memory @joshlambert
  * [ ] Database @joshlambert
  * [ ] Search @phikai @joshlambert
* [ ] Update Challenges
  * [ ] Distribution @ljlane
  * [ ] Ecosystem @deuley
  * [ ] Geo @fzimmer
  * [ ] Memory @joshlambert
  * [ ] Database @joshlambert
  * [ ] Search @phikai @joshlambert
* [ ] Update Whats Next
  * [ ] Distribution @ljlane
  * [ ] Ecosystem @deuley
  * [ ] Geo @fzimmer
  * [ ] Memory @joshlambert
  * [ ] Database @joshlambert
  * [ ] Search @phikai @joshlambert
* [ ] Final review @joshlambert @cdu1


