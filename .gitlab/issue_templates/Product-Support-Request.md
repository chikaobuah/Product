<!-- DO NOT DELETE THE BELOW LINE. This project is public, so all requests with customer data NEEDS to be confidential. -->
/confidential

<!--
How to request support from Product:
1. Set the issue title to be of the form <Company Name>: <Description of Request>
1. Fill out the fields below to the best of your ability
1. Ensure it is confidential. Do not delete the first line which sets confidential.
1. Save the issue and ensure the label "support request" is applied
-->

## Organization information

<!-- Prospect or Customer? Select the approrpriate label.-->
/label ~"account::prospect", ~"account::customer"

**Contact Name, Title**:

**Organization Name**:

## Support needed

<!-- Request type 
Apply the below three labels based on the type of support required:
* Vision: Discussion of current and upcoming features
* Prioritization: Organization has unmet needs, and needs to accelerate delivery of a feature to unlock sufficient value
* Troubleshooting: Feature is not functioning as expected -->
/label ~"type::vision", ~"type::prioritization", ~"type::troubleshooting"

<!-- Priority
How time critical is the request?
* Urgent: Higher priority. Post a message in #product, linking to this issue, and mention the responsible PM.
* Routine: Response requested within 3-4 business days. -->
/label ~"priority::urgent", ~"priority::routine"

<!-- Impact
Potential impact to the business.
* Critical: Over $1M USD TCV. Once this issue has been created, post in #product and link it. Mention the responsible PM, Director, and VP of Product.
* High: Over $500K USD TCV. Once this issue has been created, post in #product and link it. Mention the responsible PM, Director.
* Medium: Over $100K USD TCV.
* Low: Anything below $100K USD TCV.
-->
/label ~"impact::critical", ~"impact::high", ~"impact::medium", ~"impact::low"

<!-- Stage
Apply a stage label based on: https://about.gitlab.com/handbook/product/categories/
This helps the product team triage requests more quickly. If the request is not specific to a stage, select stage::none.

Stages are listed below, include just the relevant stage.
-->
/label ~"devops::manage"
/label ~"devops::plan"
/label ~"devops::create"
/label ~"devops::verify"
/label ~"devops::package"
/label ~"devops::secure"
/label ~"devops::release"
/label ~"devops::configure"
/label ~"devops::monitor"
/label ~"devops::defend"
/label ~"devops::none"

<!-- Request details
Description of the support request.
* What is the need/request? 
* What is the potential impact?
* Be sure to link to relevant issues 
* Include any important timelines, in particular for priotization requests -->

## Background

<!-- Include any relevant background on this request:
* Prior calls and history
* Links to issues which they may have commented on, or are following
* What the organization is attempting to do
* etc. -->


/label ~"support request"