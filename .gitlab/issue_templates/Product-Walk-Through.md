## Description
<!-- Describe what part of the product you are going to walk through. It is helpful to start with a topic or a docs link that you expect to follow along with.-->

## Brief Feature Overview and Expectations
<!-- Provide an overview of the feature as it is currently implemented in your own words, why a user would utilize this feature and your expectations of the walk-through. -->

## Results
<!-- Update the links below as they are available -->
* [GitLab Unfiltered Video]()
* [Notes Google Doc]()
* Follow up issues added as related issues

## Tasks
### Pre-Walk Through
* [ ] Post to shared calendar (Group, Stage or Section ones if they exist) so that other team members can attend if they wish
* [ ] Create Google Doc
* [ ] Update the "Brief Feature Overview and Expectations" section

### During Walk Through
* [ ] Record Video
* [ ] Start with verbalizing the Brief Feature Overview and Expectations section
* [ ] Capture notes for issues to file in the GoogleDoc

### After Walk Through
* [ ] Post recording to GitLab Unfiltered YouTube channel
* [ ] Share recording and this issue in appropriate #s_ and #g_ channels
* [ ] Create follow up issues and ping relevant product managers
* [ ] Attach issues as related to this one
* [ ] Close this issue