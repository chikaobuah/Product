## Common tasks for ramping up as a PM

For some of the links below, you'll need to know your group label. Typically, this will just be in the format `group::apm`. If you are unsure of your group, please check with your manager. The links as written will take you to the APM group board as an example, simply modify the label query with your own group name to see your own board. You may also subscribe to any label, which will notify you via email each time an issue with that label is updated. 

It will also be very helpful to know the contacts for your section, stage, and group. This information is on the [categories handbook page](https://about.gitlab.com/handbook/product/categories/#devops-stages), and it's where you can find out who your assigned UX designer, engineering manager, and other roles mentioned below are.

### Week 2 (After Company Onboarding)
* [ ] Complete [Product Section of your onboarding ticket](https://gitlab.com/gitlab-com/people-ops/employment/issues/1167#for-product-management-only)
* [ ] Familiarize yourself with the [product development timeline](https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline). It's important to know where we are in the cadence and start to work ahead as quickly as possible.
* [ ] Review your [Planning Board](https://gitlab.com/groups/gitlab-org/-/boards/1065731?label_name[]=group%3A%3Aapm) (remember, you'll need to update the group name in the query). Ensure you understand each issue in the currently underway release, as well as the next - you can find the milestone names for upcoming releases on the [schedule](https://about.gitlab.com/upcoming-releases/). If the problem we are solving isn't clear, ping your manager; if the proposal for how we would achieve it isn't clear ping your engineering manager. If the design is unclear, work with your product designer. Update issue descriptions for clarity. We have recorded an [issue grooming workshop](https://www.youtube.com/watch?v=es-SuhU_6Rc) that may help you here.
* [ ] Review the [Section Group Conversation Prep Issue Template](https://gitlab.com/gitlab-com/Product/blob/master/.gitlab/issue_templates/Group-Conversation-Prep.md) and submit an MR if it needs to be updated to include you in your new role. Assign the MR to your manager for review and merge.
* [ ] Attend your group's weekly meetings and introduce yourself to the team.
* [ ] Make any necessary adjustments to the planning board for the next release in coordination with with your engineering manager, send to your manager for async review.

### Week 3
* [ ] Review [recent release posts](https://about.gitlab.com/blog/categories/releases/), in particular the X.0 releases, for what's been changing in your stage.
* [ ] Review recent analyst research in #product-marketing and #analyst-relations slack channels - setup a meeting with Analyst Relations Manager Joyce Tompsett (`@Tompsett`) to introduce yourself, receive relevant research, and begin getting involved in analyst activities in your area. 
* [ ] Coordinate with your engineering manager on what will be delivered in the current release; ensure [release post content is merged](https://about.gitlab.com/handbook/marketing/blog/release-posts/#feature-blocks) in time.
* [ ] Review [the direction page](https://about.gitlab.com/direction), in particular the sub-page for your stage and the direction pages for the categories you manage. You can find links directly to your category vision pages on the categories handbook page. You will be responsible for updating and maintaining your direction page with what your group is working on and why. 
* [ ] Review the [product development flow](https://about.gitlab.com/handbook/product-development-flow/index.html) and prepare any questions you have on it for discussion with your manager in your next 1x1.
* [ ] Carefully review the various labels used in the aforementioned product development flow. Ensure you understand how `workflow::xxxxx`, `direction`, `deliverable`, and `planning priority` labels work and what they are used for.
* [ ] Ping @mallen001 to open an issue for interview training

### Week 4
* [ ] Organize your group issues into [Epics](https://about.gitlab.com/handbook/product/#meta-epics-for-longer-term-items) including [Category Maturity Plan Epics](https://about.gitlab.com/handbook/product/#maturity-plans). Don't hesitate to [close issues as needed](https://about.gitlab.com/handbook/product/#when-to-close-an-issue). 
* [ ] Set up regular check-ins in with your Internal Customers (also found on the categories handbook page); try to aim for a monthly cadence. 
* [ ] Reach out to users via popular issues and schedule user interviews to better understand the problems our users face.
* [ ] Complete interview training
/label ~onboarding ~Doing