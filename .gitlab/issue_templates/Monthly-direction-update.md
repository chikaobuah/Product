<!-- HOW TO USE THIS TEMPLATE

First, delete the headers pertaining to Sections (Ops, CI/CD) that aren't relevant for your group conversaton.

-->

Be sure to follow the most up-to-date guidance in the handbook about [managing your product plan](https://about.gitlab.com/handbook/product/#managing-your-product-plan).

Check the [product development timeline](https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline) for the due date for this content.

## Tasks (CI/CD)

When you have completed your content please assign the MR to @jlenny for review/merge.

### Update group categories and related themes

I.e.: https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/source/direction/verify/continuous_integration and https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/source/direction/cicd/themes

- [ ] Continuous Integration (@thaoyeager)
- [ ] Runner (@DarrenEastman)
- [ ] Testing (@jheimbuck_gl)
- [ ] Release Management (@jmeshell)
- [ ] Progressive Delivery (@ogolowinski)
- [ ] Package (@trizzi)

### Review maturity plans

I.e., https://about.gitlab.com/direction/maturity/ (data is in https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/categories.yml). Runner does not track maturity so is not needed here.

- [ ] Continuous Integration (@thaoyeager)
- [ ] Testing (@jheimbuck_gl)
- [ ] Release Management (@jmeshell)
- [ ] Progressive Delivery (@ogolowinski)
- [ ] Package (@trizzi)

### Record new speed runs

- [ ] Continuous Integration (@thaoyeager)
- [ ] Runner (@DarrenEastman)
- [ ] Testing (@jheimbuck_gl)
- [ ] Release Management (@jmeshell)
- [ ] Progressive Delivery (@ogolowinski)
- [ ] Package (@trizzi)

### Update stage content

I.e.: https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/source/direction/cicd/strategies

- [ ] Verify (@jlenny)
- [ ] Package (@trizzi)
- [ ] Release (@ogolowinski)

### Update root content

I.e.: https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/source/direction/cicd

- [ ] CI/CD direction (@jlenny)
- [ ] CI/CD walkthrough (@jlenny) - https://docs.google.com/document/d/1sAJY3puYnnUk9tscYSczYHUPefE3PPQ3lDpQsIbDLpg/edit

/assign @thaoyeager @DarrenEastman @jheimbuck_gl @ogolowinski @trizzi @jmeshell @jlenny