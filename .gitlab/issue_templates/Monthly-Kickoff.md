## Group Kickoff Meetings (Completed by the 16th of the month)

### Preparation
1. Schedule a monthly, livestreamed, recorded Kickoff meeting for your group
1. Ensure that the items for discussion are labeled with the `direction` label and scheduled for the upcoming milestone
1. Update the issues to ensure:
   1. The description is single-source of truth (no digging through comments required)
   1. The description contains a strong Problem Statement, Use Cases and a complete Proposal
   1. The description contains proposed designs

### Meeting
1. Consider displaying your stage or group product direction pages during this meeting
1. Try to keep the recording of the kickoff portion of this short. Even if this is scheduled as an agenda item in a regular weekly sync, please record the Kickoff portion of the discussion separately. This way others can review the kickoff playlist for all teams efficiently.
1. Review the `direction` labeled items for the milestone one-by-one, highlighting the problem statement and designs.

### After the Meeting
1. Post the meeting recording to GitLab Unfiltered, name it `GitLab XX.XX Kickoff - Stage:Group` assign it to the `XX.XX Release Kickoff` YouTube playlist. If the playlist doesn't exist yet, please create one. Include the link to a direction page, planning issue or issue board reviewed during the video in the video description.
1. Post the link to the recording in your section, stage (#s_) and group (#g_) slack channels

### Tasks
Check when video is recorded, uploaded to Unfiltered with appropriate name, assigned to the appropriate playlist, and posted in the appropriate slack channels
* [ ] Manage:Access - @jeremy
* [ ] Manage:Compliance - @mattgonzales
* [ ] Manage:Import - @jeremy
* [ ] Manage:Analytics - @jeremy
* [ ] Plan:Project Management - @gweaver
* [ ] Plan:Portfolio Management - @kokeefe
* [ ] Plan:Certify - @mjwood
* [ ] Create:Source Code - @jramsay
* [ ] Create:Knowledge - @cdybenko
* [ ] Create:Editor - @phikai
* [ ] Create:Gitaly - @jramsay
* [ ] Create:Gitter - @jramsay
* [ ] Verify:CI - @thaoyeager
* [ ] Verify:Runner - @DarrenEastman
* [ ] Verify:Testing - @jheimbuck_gl
* [ ] Package:Package - @trizzi
* [ ] Release:Progressive Delivery - @ogolowinski
* [ ] Release:Release Management - @ogolowinski
* [ ] Configure:Orchestration - @danielgruesso
* [ ] Configure:System - @nagyv.gitlab
* [ ] Monitor:APM - @dhershkovitch
* [ ] Monitor:Health - @sarahwaldner
* [ ] Secure:Static Analysis - @stkerr
* [ ] Secure:Dynamic Analysis - @derekferguson
* [ ] Secure:Composition Analysis - @NicoleSchwartz
* [ ] Defend:Runtime Application Security - @stkerr
* [ ] Defend:Threat Management - @matt_wilson
* [ ] Defend:Application Infrastructure Security - @stkerr
* [ ] Enablement:Distribution - @ljlane
* [ ] Enabelment:Geo - @fzimmer
* [ ] Enablement:Memory - @joshlambert
* [ ] Enablement:Ecosystem - @deuley
* [ ] Enablement:Search - @phikai
* [ ] Enablement:Database - @joshlambert

## Company Kickoff Meeting (Completed by the 18th of the month)

### Preparation

1. The VP of Product will be the [directly responsible individual](/handbook/people-group/directly-responsible-individuals/) for presenting and leading the meeting.
1. The format of the meeting will be that of a [panel interview](https://www.thebalancecareers.com/panel-interview-questions-and-answers-2061158) focused on the product. This will mean they they will prepare the introduction and closing thoughts.
1. If a Director of Product Management is out of office on the day of the kickoff
   call, they should arrange for another Director of Product Management, or
   their delegate, to cover that section and inform the VP of Product ahead of
   time.
 
1. We should always seek feedback on the usefulness of the Kickoff Meeting. Ensure the [Kickoff Meeting Survey](https://docs.google.com/forms/d/12-QPpvggEsqCvZnDCnuCjqP53joKwjRMPy4PH-Mqp-I/viewform?edit_requested=true)
   contains relevant questions prior the Company Kickoff Meeting.

### Meeting

1. Follow the same instructions for [live streaming a Group Conversation](/handbook/people-group/group-conversations/#livestreaming-the-call) in order to livestream to GitLab Unfiltered. People Ops Specialists can help set up the Zoom webinar if not already attached to the Kickoff calendar invite. 
1. Ensure the [survey link](https://docs.google.com/forms/d/12-QPpvggEsqCvZnDCnuCjqP53joKwjRMPy4PH-Mqp-I/viewform?edit_requested=true)
   is added to the description of the live stream.
1. The person presenting their screen should make sure they are sharing a smaller window (default YouTube resolution is 320p, so don't fill your screen on a 1080p monitor. 1/4 of the screen is about right to make things readable.)
1. The VP of Product starts the meeting by:
   * Giving a small introduction about the topic of this meeting
   * Introducing the panel interviewees and themselves
   * Reminding anyone who may be watching the video/stream about how we [plan ambitiously](#ambitious-planning).
1. During the discussion about a product section
   * The VP of Product will continue to screenshare to aid the interviewee.
   * They should also be sure to use display cues (highlighting, mouse pointer movement) to indicate where in the document we are, so nobody watching gets lost.
   * The interviewee will explaining the problem and proposal of listed items and guides the VP of Product through the individual issues. If there is a UX design or mockup available, it will be shown.
   * Each Director of Product should try to have one visual item that can be opened up and looked at.
   * Be sure to mention the individual PMs for the stages and groups within your section while reviewing the issues they've highlighted.
   * Be sure you're on do not disturb mode so audio alerts do not play.
1. The VP of Product often ends the meeting by asking viewers to take the kickoff survey, quickly highlighting several high impact issues and communicating our excitement for the
   upcoming release. Consider even using one of our popular phrases: “This will be the best release ever!”

### Tasks
1. Section Leader Prep - check when you have:
  1. Reviewed issue titles and descriptions for your section
  1. Reviewed your section's group kickoffs videos
  1. Adjust titles and ordering of group kickoff videos in the YouTube Playlist 
  1. Added an MR to update the [Kickoff page](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/source/direction/kickoff/template.html.md.erb) with links to the new kickoff videos
  * [ ] Dev Section - @ebrinkman
  * [ ] CI/CD Section - @jlenny
  * [ ] Ops Section - @kencjohnston
  * [ ] Secure & Defend Section - @ddesanto
  * [ ] Enablement Section - @joshualambert
1. VP of Product Prep - 
  * [ ] Review and adjust the ordering of the YouTube Playlist
  * [ ] Checked the survey for appropriate questions - @sfwgitlab
1. Post meeting
  * [ ] Add the livestreamed recording to the `XX.XX Release Kickoff` YouTube playlist - @sfwgitlab
  * [ ] Mention the availability of the YouTube playlist in the next company call - @sfwgitlab
